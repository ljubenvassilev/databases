import 'dart:async';
import 'dart:io';

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:databases/models/user.dart';

class DatabaseHelper {
  static final DatabaseHelper _instance = DatabaseHelper.internal();
  factory DatabaseHelper() => _instance;
  DatabaseHelper.internal();
  static Database _db;

  final String tableUsers = "users";
  final String columnId = "id";
  final String columnUsername = "username";
  final String columnPassword = "password";

  Future<Database> get db async {
    if (_db == null) {
      _db = await initDb();
    }
    return _db;
  }

  initDb() async {
    Directory documentDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentDirectory.path, 'maindb.db');
    return await openDatabase(path, version: 1, onCreate: _onCreate);
  }

  void _onCreate(Database db, int version) async {
    await db.execute("CREATE TABLE $tableUsers("
        "$columnId INTEGER PRIMARY KEY, "
        "$columnUsername TEXT, "
        "$columnPassword TEXT)");
  }

  Future<int> saveUser(User user) async {
    var dbClient = await db;
    int result = await dbClient.insert(tableUsers, user.toMap());
    return result;
  }
  
  Future<List> getAllUsers() async {
    var dbClient = await db;
    var result = await dbClient.rawQuery('SELECT * FROM $tableUsers');
    return result.toList();
  }

  Future<int> getUsersCount() async {
    var dbClient = await db;
    return Sqflite.firstIntValue(await dbClient.rawQuery('SELECT COUNT(*) FROM $tableUsers'));
  }

  Future<User> getUser(int id) async {
    var dbClient = await db;
    var result = await dbClient.rawQuery('SELECT * FROM $tableUsers WHERE $columnId = $id');
    if (result.length == 0) return null;
    return User.fromMap(result.first);
  }

  Future<int> deleteUser(int id) async {
    var dbClient = await db;
    return await dbClient.delete(tableUsers, where: '$columnId = ?', whereArgs: [id]);
  }

  Future<int> updateUser(User user) async {
    var dbClient = await db;
    return await dbClient.update(tableUsers, user.toMap(), where: 'columnId = ?', whereArgs: [user.id]);
  }

  Future close() async {
    var dbClient = await db;
    return dbClient.close();
  }
}