import 'package:databases/models/user.dart';
import 'package:databases/util/dbhelper.dart';
import 'package:flutter/material.dart';

List _users;

void main() async {
  var db = DatabaseHelper();

  int savedUser = await db.saveUser(User('James', 'Bond'));

  _users = await db.getAllUsers();
  for(int i = 0; i < _users.length; i++) {
    User user = User.map(_users[i]);
    print (user.id.toString());
    print (user.username);
    print (user.password);
  }

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: MyHomePage(title: 'Database'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.title,
          style: TextStyle(
            color: Colors.blue.shade700
          ),
        ),
        backgroundColor: Colors.greenAccent,
        centerTitle: true,
      ),
      body: Container()
    );
  }
}
